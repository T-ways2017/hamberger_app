import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'header.dart';
import 'categories.dart';
import 'humberger_list.dart';
import 'burger_details.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.transparent),
    );
    return MaterialApp(

      theme: ThemeData(
        brightness: Brightness.light,
        accentColor: Colors.deepOrange,
        cardColor: Colors.teal,
        appBarTheme: AppBarTheme(
          color: Colors.teal,
          centerTitle: true,
        ),
        bottomAppBarColor: Colors.teal,
        floatingActionButtonTheme: FloatingActionButtonThemeData(backgroundColor: Colors.orange,
        elevation: 20,
        ),

      ),


      darkTheme: ThemeData(
        brightness: Brightness.dark,
        accentColor: Colors.grey,
        cardColor: Colors.teal,
        appBarTheme: AppBarTheme(
          color: Colors.black12,
          centerTitle: true,
        ),
        bottomAppBarColor: Colors.black12,
        floatingActionButtonTheme: FloatingActionButtonThemeData(backgroundColor: Colors.orange,
          elevation: 20,
        ),

      ),


      debugShowCheckedModeBanner: false,
      home: Hamburger(),
      routes: {BurgerDetails.tag: (_){ return BurgerDetails();}},
    );
  }
}

class Hamburger extends StatefulWidget {
  // const Hamburger({Key? key}) : super(key: key);
  @override
  _HamburgerState createState() => _HamburgerState();
}

class _HamburgerState extends State<Hamburger> {
  @override
  Widget build(BuildContext context) {
    bool ligth = Theme.of(context).brightness == Brightness.light;
    return Scaffold(
      backgroundColor: ligth ? Color.fromRGBO(240, 240, 240, 1) : Color.fromRGBO(35, 35, 35, 0.5),
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: true,
            //centerTitle: true,
            title: Text('Hamburger App silver'),
            leading: IconButton(
              icon: Icon(Icons.menu),
              onPressed: null,
            ),
            actions: [
              IconButton(
                onPressed: null,
                icon: Icon(Icons.shopping_cart),
              )
            ],
          ),
          Header(),
          Categories(),
          HumbergerList(),

          HumbergerList(),
          SliverList(
            delegate: SliverChildListDelegate([
              Text(
                'Hamburger juice',
                style: TextStyle(fontSize: 222),
              ),
            ]),
          ),
        ],
      ),
      extendBody: true,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(onPressed: null,
      child: Icon(Icons.home,color: Colors.white,),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        child: Row(
          children: [

            Spacer(),
            IconButton(onPressed: null,color: Colors.white, icon: Icon(Icons.add_alarm)
            ),
            Spacer(),
            IconButton(onPressed: null, color: Colors.white, icon: Icon(Icons.turned_in)
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}
