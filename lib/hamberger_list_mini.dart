import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class HambergerListMini extends StatefulWidget {
  //const HambergerListMini({Key? key}) : super(key: key);

  @override
  _HambergerListMiniState createState() => _HambergerListMiniState();
}

class _HambergerListMiniState extends State<HambergerListMini> {
  Widget burgersImages = Container(
    height: 129,
    child: Image.asset("images/burger_2019.png"),
  );

  Widget chickeBurger = Container(
    height: 129,
    child: Image.asset("images/burger-menu-cream.png"),
  );

  int items = 5;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      margin: EdgeInsets.only(top: 10),
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: items,
          itemBuilder: (context, index) {
            bool reverse = index.isEven;
            return Stack(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: 120,
                      height: 120,
                      margin: EdgeInsets.only(left: 10),
                      child: GestureDetector(
                        onTap: () {},
                        child: Card(
                          color: Colors.deepOrange,
                          elevation: 3,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(45),
                          ),
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(left: 20),),
                  ],
                ),
              ],
            );
          }),
    );
  }
}
