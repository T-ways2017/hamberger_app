import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Header extends StatefulWidget {
  @override
  _HeaderState createState() => _HeaderState();
}

class _HeaderState extends State<Header> {
  @override
  Widget build(BuildContext context) {
    bool ligth = Theme.of(context).brightness == Brightness.light;
    Size taille = MediaQuery.of(context).size;
    return SliverList(
        delegate: SliverChildListDelegate([
      Stack(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 9),
            height: taille.height / 5,
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(color: Colors.black12, offset: Offset(0, 5))
                ],
                color: Colors.teal,
                borderRadius:
                    BorderRadius.vertical(bottom: Radius.circular(40.2))),
            child: Column(
              children: [
                Row(
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.white,
                      radius: 42,
                      child: CircleAvatar(
                        backgroundImage:
                            AssetImage("images/profil_picture.png"),
                        radius: 40,
                      ),
                    ),
                    Column(
                      children: [
                        Text(
                          "User Name",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 16),
                        ),
                        Container(
                          decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),color: Colors.orange),
                          padding: EdgeInsets.all(4),
                          child: Text("description User"),
                        ),
                      ],
                    ),
                    Spacer(),
                    Text("123 \$ ",style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),)
                  ],
                ),
              ],
            ),
          ),
          //Container(),
          Positioned(

            bottom: -1,
            child: Container(

              height: 50,
              width: taille.width*(0.99),
              child: Card(

                color: ligth ? Colors.grey : Theme.of(context).cardColor,
                margin: EdgeInsets.symmetric(horizontal: 50),
                elevation: 10,
                child: TextFormField(
                  decoration: InputDecoration(
                    labelStyle: TextStyle(color: Colors.blueGrey),
                    border: InputBorder.none,
                    labelText: "     What do you looking for ...?",
                    suffixIcon: Icon(Icons.search, color: ligth ? Colors.black54 : Colors.black26,),
                    contentPadding: EdgeInsets.only(left: 12),

                  ),
                ),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
              ),
            ),
          ),
        ],

      )
    ]));
  }
}
