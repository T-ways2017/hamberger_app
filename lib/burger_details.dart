import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_lorem/flutter_lorem.dart';
import 'package:hamberger_app/hamberger_list_mini.dart';
import 'humberger_list.dart';

class BurgerDetails extends StatefulWidget {
  // const BurgerDetails({Key? key}) : super(key: key);
  static const tag = "burgers_details";

  @override
  _BurgerDetailsState createState() => _BurgerDetailsState();
}

class _BurgerDetailsState extends State<BurgerDetails> {
  @override
  Widget build(BuildContext context) {
    bool ligth = Theme.of(context).brightness == Brightness.light;
    int counters = 3;
    String rapports = lorem(paragraphs: 4, words: 60);
    Size taille2 = MediaQuery.of(context).size;
    Widget burgersImages = Container(
      height: 129,
      child: Image.asset("images/burger_2019.png"),
    );

    Widget chickeBurger = Container(
      height: 129,
      child: Image.asset("images/burger-menu-cream.png"),
    );
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        actions: [
          IconButton(
            onPressed: null,
            icon: Icon(
              Icons.shopping_cart,
              color: Colors.white,
            ),
          ),
          IconButton(
            onPressed: null,
            icon: CircleAvatar(
              backgroundImage: AssetImage("images/profil_picture.png"),
              radius: 40,
            ),
          ),
        ],
        centerTitle: true,
        title: Text('Burger details '),
      ),
      body: Column(
        children: [
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Deliver Me my Burger from you menu",
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: Colors.orange),
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  children: [
                    //IMAGE
                    chickeBurger,
                    Spacer(),
                    Column(
                      children: [
                        Container(
                          child: Text(
                            "15 \$",
                            style: TextStyle(fontSize: 24),
                          ),
                          padding: EdgeInsets.only(right: 56),
                          decoration: BoxDecoration(
                              color: Colors.teal,
                              borderRadius: BorderRadius.circular(50)),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.star,
                              color: Colors.orange,
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.orange,
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.orange,
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.orange,
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.grey,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
      bottomSheet: BottomSheet(
        backgroundColor: Colors.teal,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(45))),
        onClosing: () {},
        builder: (context) {
          return ClipRRect(
            borderRadius: BorderRadius.vertical(
              top: Radius.circular(45),
            ),
            child: Container(
              color: Color.fromRGBO(29, 21, 29, 0.7372549019607844),
              width: taille2.width,
              height: taille2.height / 1.6,
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 23),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          " lorem upsum text about burger ",
                          style: TextStyle(
                            decorationStyle: TextDecorationStyle.dashed,
                            color: Colors.white,
                            fontSize: 23,
                          ),
                        ),
                        SizedBox(
                          height: 18,
                        ),
                        Text(
                          rapports,
                          style: TextStyle(color: Colors.white, fontSize: 9),
                        ),

                        Text(counters.toString(),style: TextStyle(fontSize: 23,fontWeight: FontWeight.bold),),

                        HambergerListMini(),
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 67,horizontal: 10),
                          child:
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(45),
                                //color: Colors.grey,
                                color: ligth ? Colors.grey : Colors.yellow,
                              ),
                              child: Row(
                                children: [
                                  IconButton(
                                    onPressed: () {
                                      //Remove
                                      setState(() {
                                        counters = (counters - 1);
                                      });
                                    },
                                    icon: Icon(Icons.remove),
                                    color: Colors.red,
                                  ),
                                  IconButton(

                                    onPressed: () {                                      //Add
                                      setState(() {
                                        counters = ( counters +1);
                                      });
                                    },
                                    icon: Icon(Icons.add,),
                                    color: Colors.red,
                                  ),

                                ],
                              ),
                            ),
                            Expanded(
                              child: Container(
                                height: 45,
                                padding: EdgeInsets.symmetric(horizontal: 5),
                                child: MaterialButton(
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(45)),
                                  child: Text(
                                    'Bye Now ',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  onPressed: () {},
                                  color: Theme.of(context).accentColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                        ),

                      ],
                    ),
                  ),
                ],
              ),
            ),
          );

        },
      ),
    );
  }
}
