import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'burger_details.dart';

class HumbergerList extends StatefulWidget {
  //const HumbergerList({Key? key}) : super(key: key);

  @override
  _HumbergerListState createState() => _HumbergerListState();
}

class _HumbergerListState extends State<HumbergerList> {
  @override
  Widget build(BuildContext context) {
    bool ligth = Theme.of(context).brightness == Brightness.light;
    int items =10;
    Widget burgersImages = Container(
      height: 129,
      child:  Image.asset("images/burger_2019.png"),
    );

    Widget chickeBurger = Container(
      height: 129,
      child:  Image.asset("images/burger-menu-cream.png"),
    );

    

    return SliverToBoxAdapter(
      child: Container(
        margin: EdgeInsets.only(top: 23),
        color: Colors.orange,
        height: 250,
        child: ListView.builder(

            itemCount: items,
            scrollDirection:Axis.horizontal,
            itemBuilder: (context, index) {
          return Stack(
            children: [
              Container(
                padding: EdgeInsets.only(left: index == 0 ? 15 :0),
                margin: EdgeInsets.only(top: 10),
                width: 159,
                height: 250,
                color: Colors.amberAccent,
                child: GestureDetector(
                  onTap: (){
                    //TODO NAVIGATOR
                  },
                  child: Card(
                    child: Column(
                      children: [
                        Text("Burgers",style: TextStyle(color: Colors.white),),
                        Spacer(),
                        Row(
                          children: [
                            Spacer(),
                            Text("Price \$",style: TextStyle(color: Colors.white),),
                            Spacer(),
                            Card(
                              color: Colors.white,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
                              child: Icon( Icons.add,color: Colors.teal),
                            ),

                          ],
                        ),

                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                bottom: 78,
                left: 12,
                child: GestureDetector(
                onTap: (){
                  Navigator.of(context).pushNamed(BurgerDetails.tag);
                },
                child: index.isEven ? chickeBurger : burgersImages,

              ),),
            ],
          );
        }),
      ),
    );
  }
}
